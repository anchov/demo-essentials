# Demo app.

Generic demo app by H. Antsov.

Based on initial demo app I wrote for SRINI (link to repo -> https://bitbucket.org/anchov/srini-demo/src).

Seldomly updated with additional functionality.

### Based on instructions:

1. Make a Spring Boot application

1.1 Create 3 users into the database (username and password)

1.1 User has to log in with one of the users (user registration does not have to be implemented)

1.2 User is displayed list of clients that he has entered. User is not allowed to see other user clients. (Sample html is provided with test assignment)

1.3 User can add and edit clients (Sample html is provided with test assignment)

1.4. When new client is saved validate all mandatory fields

1.5 Store all input data to database

1.6 Values of country select box has to be populated with data from database.

Please provide us your project source code.

Application has to run with 1 click. Use spring boot, embedded database and some database migration tool (liquibase, flyway) to execute sql scipts on startup.

### What I have added so far additionally / Diff with SRINI demo
1. Manual datasource config
2. Quartz scheduling
3. Java agent (replacing bytecode) -> run conf example given in build.gradle

## Instructions

To run the application use ` gradlew bootRun`. Application should then be available http://localhost:8080 and UI at http://localhost:8080/ui.

Provided users for login are available in `demo-db` project, 2_demo_users_table.yaml file.

Frontend users Angular which is packaged inside Spring Boot JAR -> in `demo-frontend` Gradle subproject.

DB uses in memory H2 and is set up by Liquibase -> in `demo-db` Gradle subproject.

You can access DB from http://localhost:8080/h2 -> input the JDBC url + credentials from properties file.