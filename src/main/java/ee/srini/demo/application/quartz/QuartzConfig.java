package ee.srini.demo.application.quartz;

import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;

@Configuration
public class QuartzConfig {

    @Bean
    @QuartzDataSource
    public DataSource quartzDataSource(DataSource demoDataSource) {
        return demoDataSource;
    }

}
