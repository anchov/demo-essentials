package ee.srini.demo.application.quartz;

import ee.srini.demo.feature.client.ClientService;
import ee.srini.demo.feature.client.data.ClientEntity;
import lombok.extern.log4j.Log4j2;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Log4j2
@Component
public class SampleJob implements Job {

    private ClientService clientService;

    @Autowired
    public SampleJob(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<ClientEntity> clients = clientService.getClients();
        log.info(Arrays.toString(clients.toArray()));
    }
}
