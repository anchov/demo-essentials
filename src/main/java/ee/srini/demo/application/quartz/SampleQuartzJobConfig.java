package ee.srini.demo.application.quartz;

import lombok.extern.log4j.Log4j2;
import org.quartz.JobDetail;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Log4j2
@Configuration
public class SampleQuartzJobConfig {

//    @Autowired
//    public SampleQuartzJobConfig(
//            SchedulerFactoryBean quartzScheduler,
//            JobDetail sampleJobDetail,
//            Trigger sampleJobDetailTrigger) {
//        Scheduler scheduler = quartzScheduler.getScheduler();
//        try {
//            scheduler.scheduleJob(sampleJobDetail, sampleJobDetailTrigger);
//        } catch (SchedulerException e) {
//            log.warn("Scheduling failed", e);
//        }
//    }

    @Bean
    public JobDetailFactoryBean sampleJobDetail() {
        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(SampleJob.class);
        jobDetailFactory.setDescription("Invoke Sample Job service...");
        jobDetailFactory.setDurability(true);
        return jobDetailFactory;
    }

    @Bean
    public CronTriggerFactoryBean sampleJobDetailTrigger(JobDetail sampleJobDetail) {
        CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
        trigger.setJobDetail(sampleJobDetail);
        trigger.setCronExpression("0 * * ? * *");
        return trigger;
    }
}
