package ee.srini.demo.application.db;

import com.zaxxer.hikari.HikariConfig;
import ee.srini.demo.feature.DemoDataSourceEntityPackageScan;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.TransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "demoEntityManagerFactory", basePackageClasses = {DemoDataSourceEntityPackageScan.class}, transactionManagerRef = "demoTransactionManager")
public class DemoDataSourceConfig {

    @Primary
    @Bean
    public DataSource demoDataSource(HikariConfig demoDataSourceHikariConfig) {
        return new DemoDataSource(demoDataSourceHikariConfig);
    }

    @Bean
    public HikariConfig demoDataSourceHikariConfig(DemoDataSourceConfigProperties demoDataSourceConfigProperties) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(demoDataSourceConfigProperties.getUrl());
        hikariConfig.setUsername(demoDataSourceConfigProperties.getUsername());
        hikariConfig.setPassword(demoDataSourceConfigProperties.getPassword());
        return hikariConfig;
    }

    @Bean
    public TransactionManager demoTransactionManager(EntityManagerFactory demoEntityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(demoEntityManagerFactory);
        return transactionManager;
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean demoEntityManagerFactory(DataSource demoDataSource,
                                                                           EntityManagerFactoryBuilder entityManagerBuilder) {
        return entityManagerBuilder
                .dataSource(demoDataSource)
                .packages(DemoDataSourceEntityPackageScan.class)
                .persistenceUnit("demo")
                .properties(jpaProperties())
                .build();
    }

    private Map<String, Object> jpaProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
        props.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
        return props;
    }
}
