package ee.srini.demo.application.db;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "demo.datasource")
public class DemoDataSourceConfigProperties {
    private String url;
    private String username;
    private String password;

}

