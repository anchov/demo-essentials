package ee.srini.demo.application.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

class DemoDataSource extends HikariDataSource {

    DemoDataSource(HikariConfig demoDataSourceHikariConfig) {
        super(demoDataSourceHikariConfig);
    }
}
