package ee.srini.demo.feature.country;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class CountryDto {
    private String country;

}
