package ee.srini.demo.feature.instrumentation;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Used for Java agent learning purposes
 */
@Log4j2
@RestController
public class JavaAgentTestController {

    @GetMapping("/instrumentationTest")
    public String instrumentationTestController() {
        return "asdf";
    }
}
