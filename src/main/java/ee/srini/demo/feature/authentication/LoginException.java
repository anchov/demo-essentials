package ee.srini.demo.feature.authentication;

public class LoginException extends RuntimeException {

    LoginException(String message) {
        super(message);
    }
}
