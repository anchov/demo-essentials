package ee.srini.demo.feature.user.data;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users", schema = "demo")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String email;
    private String password;
}