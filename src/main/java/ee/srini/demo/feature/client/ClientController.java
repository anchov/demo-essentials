package ee.srini.demo.feature.client;

import ee.srini.demo.feature.authentication.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
public class ClientController {

    private final ClientService clientService;
    private final AuthenticationService authenticationService;

    @Autowired
    public ClientController(ClientService clientService, AuthenticationService authenticationService) {
        this.clientService = clientService;
        this.authenticationService = authenticationService;
    }

    @GetMapping("/clients/{clientId}")
    public ClientDto getClient(@PathVariable Integer clientId) {
        return clientService.getClient(clientId, authenticationService.getAuthenticatedUser().getId());
    }

    @GetMapping("/clients")
    public List<ClientDto> getClients() {
        return clientService.getUsersClients(authenticationService.getAuthenticatedUser().getId());
    }

    @PostMapping("/clients")
    public ResponseEntity createClient(@Valid @RequestBody ClientDto client) {
        clientService.createClient(client, authenticationService.getAuthenticatedUser());
        return ResponseEntity.ok().build();
    }

    @PutMapping("/clients")
    public ClientDto getClient(@Valid @RequestBody ClientDto client) {
        return clientService.updateClient(client, authenticationService.getAuthenticatedUser());
    }
}