package ee.srini.demo.feature.client.data;

import ee.srini.demo.feature.user.data.UserEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "clients", schema = "demo")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private UserEntity user;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private String country;
}