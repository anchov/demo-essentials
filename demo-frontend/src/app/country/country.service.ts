import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {BackendApiService} from "../app-core/backend-api.service";

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private countriesUrl: string;

  constructor(private backendApiService: BackendApiService) {
    this.countriesUrl = '/countries';
  }

  public getAll(): Observable<String[]> {
    return this.backendApiService.get<String[]>(this.countriesUrl);
  }
}
