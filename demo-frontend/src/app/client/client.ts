export class Client {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  address: string;
  country: string;
}
