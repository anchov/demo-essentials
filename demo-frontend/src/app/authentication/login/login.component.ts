import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from "../authentication.service";
import {NotificationService} from "../../app-core/notification/notification.service";

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {

  credentials = {username: '', password: ''};

  constructor(private authenticationService: AuthenticationService,
              private notificationService: NotificationService,
              private router: Router) {
  }

  login() {
    this.authenticationService.login(this.credentials.username, this.credentials.password).subscribe(
      successData => this.router.navigateByUrl('/clients'),
      errorData => {
      });
  }
}
