import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {BackendApiService} from "../app-core/backend-api.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private backendApiService: BackendApiService) {
  }

  public login(username: String, password: String): Observable<void> {
    return this.backendApiService.post<void>("/login", {username: username, password: password});
  }

  public logout(): Observable<void> {
    return this.backendApiService.post<void>("/logout", null);
  }

}
