import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly errorsSubject$ = new Subject<string>();

  public getError$() {
    return this.errorsSubject$.asObservable();
  }

  public showError(message: string) {
    this.errorsSubject$.next(message);
  }

  public clearError() {
    this.errorsSubject$.next();
  }
}
